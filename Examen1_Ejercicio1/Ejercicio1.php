<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1 Examen</title>
</head>
<body>
        <?php
            $diccionario = array(
                'ES' =>array('0'=>"Lunes",'1'=>"Martes",'2'=>"Miercoles",'3'=>"Jueves",
                '4'=> "Viernes",'5'=>"Sabado",'6'=>"Domingo"), 
                'EN' =>array('0'=>"MONDAY",'1'=>"TUESDAY",'2'=>"WEDNESDAY",
                '3'=>"THURSDAY",'4'=>"FRIDAY",'5'=>"SATURDAY",'6'=>"SUNDAY")
        
            );

            
        ?>

        <form action="Ejercicio1.php" method="post">
            <h2>Traductor español-ingles</h2>
                <select name="selector">
                    <?php
                        $contador=0;
                        foreach ($diccionario as $idioma => $i) {
                            foreach ($i as $key =>$value) {
                                if ($idioma=="ES") {
                                    echo "<option value='".$contador."'>".$value."</option>";
                                }
                                $contador++;
                            }
                        }
            
                    ?>
                </select> 
                
                <input type="text" name="campoPalabra">
        
            <input type="submit" name="comprobar" value="comprobar" >
        </form>

        <?php

            if (isset($_POST['comprobar'])) {
                $palabraIngles=strtoupper($_POST['campoPalabra']);
                $palabraEspanol=$_POST['selector'];
                $indicador=false;
                
                
                foreach ($diccionario as $idioma => $i) {
                    foreach ($i as $key =>$value) {
                        if ($idioma=="EN") {
                            if ($value==$palabraIngles) {
                                $indicador=true;
                            }
                        }
                        
                    }
                }
                if ($indicador==true) {
                    echo "<p>!!!CORRECTO!!!</p>";
                }else{
                    echo "<p>!!!INCORRECTO!!!</p>";
                }
            }


        ?>

</body>
</html>