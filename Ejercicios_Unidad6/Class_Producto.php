<?php 

    class Producto{
        protected $codigo;
        protected $precio;
        protected $nombre;
        protected $categoria;

       public function __construct($codigo,$precio,$nombre,$categoria)
       {
        $this->codigo=$codigo;
        $this->precio=$precio;
        $this->nombre=$nombre;
        $this->categoria=$categoria;
           
       } 


       
        public function getCategoria()
        {
                return $this->categoria;
        }

       
        public function setCategoria($categoria)
        {
                $this->categoria = $categoria;

                return $this;
        }

       
        public function getNombre()
        {
                return $this->nombre;
        }

     
         
        public function setNombre($nombre)
        {
                $this->nombre = $nombre;

                return $this;
        }

      
        public function getPrecio()
        {
                return $this->precio;
        }

     
        public function setPrecio($precio)
        {
                $this->precio = $precio;

                return $this;
        }

      
        public function getCodigo()
        {
                return $this->codigo;
        }

     
        public function setCodigo($codigo)
        {
                $this->codigo = $codigo;

                return $this;
        }
    }



?>