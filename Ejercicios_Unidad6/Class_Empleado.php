<?php
    class Empleado{

        protected $nombre;
        protected $dni;
        protected $sueldo;

        public function __construct($nombre,$dni,$sueldo)
        {
            $this->nombre=$nombre;
            $this->dni=$dni;
            $this->sueldo=$sueldo;
        }

        public function setSueldo($valor) {
            $this->sueldo=$valor;
        }
        
        public function getSueldo() {
           return $this->sueldo;  
        }
        
        public function setNombre($valor) {
            $this->nombre=$valor;
        }
        
        public function getNombre() {
           return $this->nombre;  
        }


        public function mostrarEmpleado(){
            return "El empleado ".$this->nombre." con dni "
            .$this->dni." gana ".$this->sueldo." euros al mes";
        }

    }








?>