<?php 
	
	define("HOST", "localhost");
	define("USERNAME", "dwes");
	define("PASSWORD", "dwes");
	define("DATABASE", "basedatosejercicio");


	function getConexionMSQLI(){
		$conexion = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);
		$conexion->set_charset("utf8");
		$error = $conexion->connect_errno;
		if ($error != null){
			 print "<p>Se ha producido el error: $conexion->connect_error.</p>";
			 exit();
		}

		return $conexion;
	}


	function getConexionPDO(){
		$opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
		$conexion = new PDO('mysql:HOST=127.0.0.1;dbname=basedatosejercicio', 'dwes',
		'dwes' , $opciones);

		return $conexion;
	}

 ?>