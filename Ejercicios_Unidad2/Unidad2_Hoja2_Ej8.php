<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Unidad 2 Hoja 2 Ejercicio 8</title>
</head>
<body>
        <?php
            $num=35;

            $billetesDe10=0;
            $billetesDe5=0;
            $monedasDe1=0;

            
            $billetesDe10=intval($num/10);
            $num=$num%10;
            $billetesDe5=intval($num/5);
            $num=$num%5;
            $monedasDe1=intval($num/1);

            echo "La cantidad se desglosa en ".$billetesDe10." billetes de Diez, ".$billetesDe5."
             billetes de Cinco y ".$monedasDe1." monedas de 1€";

        ?>
</body>
</html>