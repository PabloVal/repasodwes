<?php

    require_once "conexionBDMSQLI.php";


    function comprobarUsuari($dni){
        $conexion=getConexionMSQLI();
        $consulta=$conexion->stmt_init();
        $consulta->prepare("SELECT * from usuarios where dni=?");
        $consulta->bind_param('s',$dni);
        $consulta->execute();
        $consulta->bind_result($usuario);
        if ($usuario==null) {
            return false;
        }else{
            return true;
        }

    }

    function getCategorias(){
        $categorias = array();
        $conexion=getConexionMSQLI();
           $busqueda=$conexion->query("SELECT nombre,precio FROM viajes ORDER BY precio");
           $viaje=$busqueda->fetch_object();
           while ($viaje != null) {
            $viajes[]=array("nombre"=>$viaje->nombre,
                                 "precio"=>$viaje->precio,);
            $viaje=$busqueda->fetch_object();
        }
    
         $busqueda->close();
    
           return $viajes;
    }


?>