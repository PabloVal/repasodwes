<?php

    require_once "conexionBDPDO.php";

     function getUsuario($dni){
		
        $conexion = getConexionPDO();
		$consulta = $conexion->prepare('SELECT * from usuarios WHERE dni:?');
        $consulta->bindParam(1,$dni);
		$consulta->execute();
		$usuario = $consulta->fetch();
		$usuarios = array();
        
		while ($usuario != null) {
            $nuevoUsuario=$usuario;
		    array_push($usuarios, $nuevoUsuario);
		    $usuario = $consulta->fetch();
		    
		}
		
		return $usuarios;
    }


?>