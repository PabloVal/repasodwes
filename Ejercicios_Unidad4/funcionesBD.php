
<?php

require_once ("conexionBD.php");

function getEquiposMySqli(){
    $equipos = array();
    $conexion=getConexion();
    $consulta='SELECT nombre FROM equipos';
    if($resultado=$conexion->query($consulta)){
		
        while ($equipo =$resultado->fetch_object()) {
            
            $equipos[]=$equipo->nombre;
            
        }
        
        $resultado->close();

    }

    return $equipos;
}


function getJugadoresEquipo($equipo){
    $jugadores = array();
    $conexion=getConexion();
    $consulta='SELECT nombre FROM jugadores WHERE jugadores.nombre_equipo="'.$equipo.'"';
        if ($resultado=$conexion->query($consulta)) {
            
            while ($jugador=$resultado->fetch_object()) {
                
                $jugadores[]=$jugador->nombre;

            }
            $resultado->close();
        }

    return $jugadores;
}

function buscarNombre_YPeso($equipo){
    $jugadores = array();
    $conexion=getConexion();
       $busqueda=$conexion->query("SELECT nombre, peso, codigo FROM jugadores WHERE nombre_equipo='$equipo'");
       $jugador=$busqueda->fetch_object();

       while ($jugador != null) {
        $jugadores[]=array("codigo"=>$jugador["codigo"], "nombre"=>$jugador["nombre"], "peso"=>$jugador["peso"]);
        $jugador=$busqueda->fetch_object();
    }

    /*También se puede utilizar el fetch_array, es lo mismo
      que usar el fetch_object que te devuelve un objeto con las claves y demás 
    */

     $busqueda->close();


       
       return $jugadores;

}


?>