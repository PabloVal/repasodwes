<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1,Hoja2 Unidad4</title>
</head>
<body>

        <?php
            include_once "funcionesBD_Libros.php";
         ?>

        <form action="Unidad4_Hoja2_Ej1.php" method="post">

        <label>Inserte los datos del libro</label>

            <label>Titulo</label>
                <input type="text" name="campoNombre">
            <br><br>

            <label>Año De Edicion</label>
                <input type="number" name="campoAnoEdicion">
            <br><br>

            <label>Precio</label>
                <input type="number" step="0.01" name="campoPrecio">
            <br><br>

            <label>Fecha de Adquisicion</label>
                <input type="date" name="campoFecha">        
            <br><br>

            <input type="submit" name="guardar" value="Guardar datos del libro">
            <br><br>
            <a href="libros_datos.php">Mostrar los libros de la Base de Datos</a>

        </form>

            <?php

            if (isset($_POST['guardar'])) {
                $titulo=$_POST['campoNombre'];
                $annioEdicion=$_POST['campoAnoEdicion'];
                $precio=$_POST['campoPrecio'];
                $fecha_adquisicion=$_POST['campoFecha'];

                añadirLibro($titulo,$annioEdicion,$precio,$fecha_adquisicion);

            }

        
        ?>


</body>
</html>