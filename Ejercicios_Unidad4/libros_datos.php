<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
        width: 50%;
        border: 1px solid #000;
        }
        th, td {
        width: 25%;
        text-align: left;
        vertical-align: top;
        border: 1px solid #000;
        border-spacing: 0;
        }
    
    </style>
</head>
<body>

        <?php
            include_once "funcionesBD_Libros.php";
            $libros=getTodoLibros();
            //var_dump(array_keys($libros));

            echo "<table>
            <tr>

                ";
            
            foreach (array_Keys($libros[0]) as $value) {
                    echo "<th>".$value."</th>";
            }
            
            echo "</tr>";
            

            foreach ($libros as $libro) {
                echo "<tr>";
                echo "<td>".$libro["numero_ejemplar"]."</td>";
                echo "<td>".$libro["titulo"]."</td>";
                echo "<td>".$libro["ano_edicion"]."</td>";
                echo "<td>".$libro["precio"]."</td>";
                echo "<td>".$libro["fecha_adquisicion"]."</td>";
                echo "</tr>";
            }
            
            echo "</table>";

        ?>
</body>
</html>