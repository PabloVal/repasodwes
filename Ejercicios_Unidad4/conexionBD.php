<?php 
	
	define("HOST", "localhost");
	define("USERNAME", "root");
	define("PASSWORD", "");
	define("DATABASE", "dwes_02_libros");


	function getConexion(){
		$conexion = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);
		$conexion->set_charset("utf8");
		$error = $conexion->connect_errno;
		if ($error != null){
			 print "<p>Se ha producido el error: $conexion->connect_error.</p>";
			 exit();
		}

		return $conexion;
	}

 ?>