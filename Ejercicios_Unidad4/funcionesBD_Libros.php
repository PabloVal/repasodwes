
<?php

require_once ("conexionBD.php");

function añadirLibro($t,$annio,$p,$ad){
    $conexion=getConexion();
    $consulta = $conexion->stmt_init();
    $consulta->prepare('INSERT INTO libro (titulo, ano_edicion,precio,fecha_adquisicion) 
    VALUES (?,?,?,?)');
    $consulta->bind_param('sids',$t,$annio,$p,$ad);
    $consulta->execute();
    $consulta->close();
   
}


function getTodoLibros(){
    $libros = array();
    $conexion=getConexion();
       $busqueda=$conexion->query("SELECT numero_ejemplar,titulo,ano_edicion,precio,fecha_adquisicion FROM libro");
       $libro=$busqueda->fetch_object();
       while ($libro != null) {
        $libros[]=array("numero_ejemplar"=>$libro->numero_ejemplar,"titulo"=>$libro->titulo,"ano_edicion"=>$libro->ano_edicion,
         "precio"=>$libro->precio,"fecha_adquisicion"=>$libro->fecha_adquisicion);
        $libro=$busqueda->fetch_object();
    }

     $busqueda->close();

       return $libros;
}



?>